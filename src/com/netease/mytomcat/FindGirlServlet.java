package com.netease.mytomcat;

import java.io.IOException;

/**
 * 测试servelt之一
 */
public class FindGirlServlet extends MyServelet {

    @Override
    public void doGet(MyRequest request, MyResponse response) {
        try {
            response.write("Find a girl in doGet Method");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(MyRequest request, MyResponse response) {
        try {
            response.write("Find a girl in doPOST Method");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
