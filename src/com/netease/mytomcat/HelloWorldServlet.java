package com.netease.mytomcat;

import java.io.IOException;

/**
 * 测试servelt之二
 */
public class HelloWorldServlet extends MyServelet{
    @Override
    public void doGet(MyRequest request, MyResponse response) {
        try {
            response.write("Say Hello world in doGet Method");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(MyRequest request, MyResponse response) {
        try {
            response.write("Say Hello world in doPOST Method");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
