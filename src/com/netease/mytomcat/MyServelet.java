package com.netease.mytomcat;

/**
 * Tomcat是满足Servlet规范的容器，那么自然Tomcat需要提供API。
 * 这里是Servlet常见的doGet/doPost/service方法。
 */
public abstract class MyServelet {

    public void service(MyRequest request, MyResponse response) {
        if (request.getMethod().equalsIgnoreCase("POST")) {
            doPost(request, response);
        } else if (request.getMethod().equalsIgnoreCase("GET")) {
            doGet(request, response);
        }
    }

    public void doGet(MyRequest request, MyResponse response) {

    }

    public void doPost(MyRequest request, MyResponse response) {

    }
}
