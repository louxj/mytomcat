package com.netease.mytomcat;

public class ServeletMapping {
    private String serveltName;

    private String url;

    private String clazz;

    public ServeletMapping(String serveltName, String url, String clazz) {
        this.serveltName = serveltName;
        this.url = url;
        this.clazz = clazz;
    }

    public String getServeltName() {
        return serveltName;
    }

    public void setServeltName(String serveltName) {
        this.serveltName = serveltName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }
}
